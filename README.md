# JNI sample

This is a very simple example of how JNI works.

1. Run `make` to compile everything
2. Run `LD_LIBRARY_PATH=. java JavaCode <text>` to run the compiled
   executables
