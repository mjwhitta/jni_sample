class JavaCode {
    static {
        System.loadLibrary("cpp_code");
    }

    public static void main(String[] args) {
        for (String str : args) {
            new JavaCode().print_java_string(str);
        }
    }

    private native void print_java_string(String str);
}
