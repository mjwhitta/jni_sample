#include <iostream>
#include "jni.h"
#include "cpp_code.h"

using namespace std;

JNIEXPORT void JNICALL Java_JavaCode_print_1java_1string(
    JNIEnv* env,
    jobject obj,
    jstring java_str
) {
    const char* native_str = env->GetStringUTFChars(java_str, 0);
    cout << native_str << endl;
    env->ReleaseStringUTFChars(java_str, native_str);
    return;
}
