all: cpp

check:
	@which g++ >/dev/null
	@which javac >/dev/null
	@which javah >/dev/null

clean:
	@rm -rf *.h *.so *.class

clena: clean

cpp: java
	g++ -fPIC -I $$JAVA_HOME/include -I $$JAVA_HOME/include/linux \
	    -o libcpp_code.so -shared cpp_code.cpp

java: check
	javah JavaCode
	javac JavaCode.java
	@mv JavaCode.h cpp_code.h
